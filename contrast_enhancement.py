from sys import argv	# imports class sys (contains useful sytem level functions)
import Image # class useful for image processing
import numpy as np

def process_image(argv): 
	im1 = Image.open(argv[1]);
	im2 = Image.open(argv[2]);
	im3 = Image.open(argv[3]);
	im4 = Image.open(argv[4]);

	# Loading images to memory 
	im1_array = im1.load()
	im2_array = im2.load()
	im3_array = im3.load()
	im4_array = im4.load()
 	nx_pixels, ny_pixels = im1.size	

	img = np.zeros((ny_pixels,nx_pixels,4), dtype=np.uint8)
	for y in range(ny_pixels):
		for x in range(nx_pixels):
			img[y][x][0] = int(im1_array[x,y])
			img[y][x][1] = int(im2_array[x,y])
			img[y][x][2] = int(im3_array[x,y])
			img[y][x][3] = int(im4_array[x,y])

	for z in range(4):
		min_array = np.amin(img[:,:,z])
		max_array = np.amax(img[:,:,z])
		for y in range(ny_pixels):
			for x in range(nx_pixels):
				img[y][x][z] = int(((img[y][x][z] - min_array + 0.0)/(max_array - min_array))*255)
		im = Image.fromarray(img[:,:,z])
		im.save("CS_band"+str(z+1)+".gif")	


def main():

	if (len(argv)!=5):
		print "Incorrect command line arguments: exiting"
		sys.exit()
	
	process_image(argv)

main()


