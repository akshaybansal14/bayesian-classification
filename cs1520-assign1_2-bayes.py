from sys import argv	# imports class sys (contains useful sytem level functions)
import sys
import Image # class useful for image processing
import numpy as np
from scipy.stats import multivariate_normal

def input_training_dataset(class1, class2, argv):

	file1 = open(argv[1],'r')
	file2 = open(argv[2],'r')

	for line in file1:
		class1.append([int(p) for p in line.split()])

	for line in file2:
		class2.append([int(p) for p in line.split()])

	file1.close()
	file2.close()

	im1 = Image.open("CS_band1.gif");
	im2 = Image.open("CS_band2.gif");
	im3 = Image.open("CS_band3.gif");
	im4 = Image.open("CS_band4.gif");

	# Loading images to memory 
	im1_array = im1.load()
	im2_array = im2.load()
	im3_array = im3.load()
	im4_array = im4.load()
 	nx_pixels, ny_pixels = im1.size	

	img = np.zeros((ny_pixels,nx_pixels,4), dtype=np.uint8)
	for y in range(ny_pixels):
		for x in range(nx_pixels):
			img[y][x][0] = int(im1_array[x,y])
			img[y][x][1] = int(im2_array[x,y])
			img[y][x][2] = int(im3_array[x,y])
			img[y][x][3] = int(im4_array[x,y])

	return(img)

def classification(class1, class2, img):

	npts_class1 = len(class1)
	npts_class2 = len(class2)
	class1_data = [[] for i in range(npts_class1)]
	class2_data = [[] for i in range(npts_class2)]
	for i in range(npts_class1):
		for k in range(4):
			class1_data[i].append(img[class1[i][0]][class1[i][1]][k])
	for i in range(npts_class2):
		for k in range(4):
			class2_data[i].append(img[class2[i][0]][class2[i][1]][k])
	
	mu_class1 = np.array([0,0,0,0])	
	mu_class2 = np.array([0,0,0,0])
	for k in range(4):
		for i in range(npts_class1):
			mu_class1[k] += class1_data[i][k]
		for j in range(npts_class2):
			mu_class2[k] += class2_data[j][k]	
	mu_class1 = mu_class1 *1.0 / npts_class1
	mu_class2 = mu_class2 *1.0 / npts_class2

	var_covar_class1 = np.zeros((4,4))
	var_covar_class2 = np.zeros((4,4))
	nb_var_covar_class1 = np.zeros((4,4))
	nb_var_covar_class2 = np.zeros((4,4))
	for i in range(4):
		for j in range(i,4):
			for p in range(npts_class1):
				var_covar_class1[i][j] += (class1_data[p][i] - mu_class1[i])*(class1_data[p][j] - mu_class1[j])
			var_covar_class1[j][i] = var_covar_class1[i][j]
			if (i == j):
				nb_var_covar_class1[i][j] = var_covar_class1[i][j]
			
	for i in range(4):
		for j in range(i,4):
			for p in range(npts_class2):
				var_covar_class2[i][j] += (class2_data[p][i] - mu_class2[i])*(class2_data[p][j] - mu_class2[j])
			var_covar_class2[j][i] = var_covar_class2[i][j]
			if (i == j):
				nb_var_covar_class2[i][j] = var_covar_class2[i][j]



	var_covar_class1 = var_covar_class1 *1.0 / npts_class1
	var_covar_class2 = var_covar_class2 *1.0 / npts_class2
	nb_var_covar_class1 = nb_var_covar_class1 *1.0 / npts_class1
	nb_var_covar_class2 = nb_var_covar_class2 *1.0 / npts_class2

	# Bayes and Naive Bayes Classification (Assignment I and II)
	var_class1 = multivariate_normal(mu_class1, var_covar_class1)
	var_class2 = multivariate_normal(mu_class2, var_covar_class2)
	nb_var_class1 = multivariate_normal(mu_class1, nb_var_covar_class1)
	nb_var_class2 = multivariate_normal(mu_class2, nb_var_covar_class2)
	
	n_choice = 3
	classified_image_bayes = np.zeros((512,512,n_choice), dtype=np.uint8)
	classified_image_nbayes = np.zeros((512,512,n_choice), dtype=np.uint8)
	for y in range(512):
		for x in range(512):
			curr_pt = []
			for k in range(4):
				curr_pt.append(img[y][x][k])
			bayes_ratio = var_class1.pdf(curr_pt)/var_class2.pdf(curr_pt)
			nbayes_ratio = nb_var_class1.pdf(curr_pt)/nb_var_class2.pdf(curr_pt)
			for i in range(n_choice):
				P = float(argv[i+3])
				if(bayes_ratio > ((1-P)/P)):
					classified_image_bayes[y][x][i] = 0
				else:
					classified_image_bayes[y][x][i] = 255
				if(nbayes_ratio > ((1-P)/P)):
					classified_image_nbayes[y][x][i] = 0
				else:
					classified_image_nbayes[y][x][i] = 255
	for i in range(n_choice):
		image_classified_bayes = Image.fromarray(classified_image_bayes[:,:,i])
		image_classified_bayes.save("Bayes_classifier(P="+argv[i+3]+").gif")
		image_classified_nbayes = Image.fromarray(classified_image_nbayes[:,:,i])
		image_classified_nbayes.save("Naive_Bayes_classifier(P="+argv[i+3]+").gif")		

	all_list = [class1_data, class2_data, img]
	return(all_list)



def main():

	if (len(argv)!=6):
		print "Incorrect command line arguments: exiting"
		print "Usage: python "+argv[0]+" [Training set class I] [Training set class II] [P1] [P2] [P3]"
		sys.exit()



	class1 = [] 	# List of pixel coordinates representing land
	class2 = []		# List of pixel coordinates representing water
	img = input_training_dataset(class1, class2, argv)
	all_list = classification(class1, class2, img)

main()


