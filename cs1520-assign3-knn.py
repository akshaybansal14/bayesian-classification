from sys import argv	# imports class sys (contains useful sytem level functions)
import sys
import Image # class useful for image processing
import numpy as np
from scipy.stats import multivariate_normal
from operator import itemgetter
import math

def input_training_dataset(class1, class2, argv):

	file1 = open(argv[1],'r')
	file2 = open(argv[2],'r')

	for line in file1:
		class1.append([int(p) for p in line.split()])

	for line in file2:
		class2.append([int(p) for p in line.split()])

	file1.close()
	file2.close()

	im1 = Image.open("CS_band1.gif");
	im2 = Image.open("CS_band2.gif");
	im3 = Image.open("CS_band3.gif");
	im4 = Image.open("CS_band4.gif");

	# Loading images to memory 
	im1_array = im1.load()
	im2_array = im2.load()
	im3_array = im3.load()
	im4_array = im4.load()
 	nx_pixels, ny_pixels = im1.size	

	img = np.zeros((ny_pixels,nx_pixels,4), dtype=np.uint8)
	for y in range(ny_pixels):
		for x in range(nx_pixels):
			img[y][x][0] = int(im1_array[x,y])
			img[y][x][1] = int(im2_array[x,y])
			img[y][x][2] = int(im3_array[x,y])
			img[y][x][3] = int(im4_array[x,y])

	return(img)

def knn_classifier(class1, class2, img):

	npts_class1 = len(class1)
	npts_class2 = len(class2)
	class1_data = [[] for i in range(npts_class1)]
	class2_data = [[] for i in range(npts_class2)]
	for i in range(npts_class1):
		for k in range(4):
			class1_data[i].append(img[class1[i][0]][class1[i][1]][k])
	for i in range(npts_class2):
		for k in range(4):
			class2_data[i].append(img[class2[i][0]][class2[i][1]][k])
	dist_class = [[] for i in range(npts_class1+npts_class2)]
	curr_pt = [0,0,0,0]
	for i in range(npts_class1+npts_class2):
		dist_class[i] = [0.0,0]

	n_images = len(argv)-3
	dim = 4	
	classified_image = np.zeros((512,512,n_images), dtype=np.uint8)

	for y in range(512):
		for x in range(512):
			for z in range(dim):
				curr_pt[z] = img[y][x][z]

			for i in range(npts_class1):
				temp = 0
				for p in range(dim):
					temp += (int(curr_pt[p]) - int(class1_data[i][p]))**2
				dist_class[i][0] = math.sqrt(temp)		
				dist_class[i][1] = 0
			for i in range(npts_class2):
				temp = 0
				for p in range(dim):
					temp += (int(curr_pt[p]) - int(class2_data[i][p]))**2
				dist_class[i+npts_class1][0] = math.sqrt(temp)
				dist_class[i+npts_class1][1] = 1

			dist_class = sorted(dist_class, key=itemgetter(0))
			for t in range(n_images):
				k = int(argv[t+3])
				num_class2 = sum(dist_class[q][1] for q in range(k))
				if (num_class2 > int(k/2)):
					classified_image[y][x][t] = 255
				else:
					classified_image[y][x][t] = 0
	for t in range(n_images):
		image_classified = Image.fromarray(classified_image[:,:,t])
		image_classified.save("knn_classifier(k="+argv[t+3]+").gif")	


			
def main():

	if (len(argv)<=3):
			print "Incorrect command line arguments: exiting"
			print "Usage: python "+argv[0]+" [Training set class I] [Training set class II] [k-values]"
			sys.exit()

	class1 = [] 	# List of pixel coordinates representing land
	class2 = []		# List of pixel coordinates representing water
	img = input_training_dataset(class1, class2, argv)
	knn_classifier(class1, class2, img)

main()


